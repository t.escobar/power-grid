package main

import (
	"testing"
)

func BenchmarkCopy(b *testing.B) {
	s := make([]int8, 100)
	for i := 0; i < len(s); i++ {
		s[i] = int8(i)
	}

	b.Run("copy with range iteration", func(bb *testing.B) {
		ns := make([]int8, 100)

		for i := 0; i < bb.N; i++ {
			for j, e := range s {
				ns[j] = e
			}
		}
	})

	b.Run("copy with index iteration", func(bb *testing.B) {
		ns := make([]int8, 100)

		for i := 0; i < bb.N; i++ {
			for j := 0; j < len(s); j++ {
				ns[j] = s[j]
			}
		}
	})

	b.Run("copy with copy() function", func(bb *testing.B) {
		ns := make([]int8, 100)

		for i := 0; i < bb.N; i++ {
			copy(ns, s)
		}
	})

	b.Run("binomial", func(bb *testing.B) {
		for i := 0; i < bb.N; i++ {
			_ = make([]int8, 137846528820)
		}
	})
}

func BenchmarkMainTest(b *testing.B) {
	for i := 0; i < b.N; i++ {
		run("../input/grid-25-40")
	}
}
