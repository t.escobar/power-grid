package main

import (
	"bufio"
	"fmt"
	"golang.org/x/sync/semaphore"
	"os"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

// Vertices is the list of vertices
type Vertices []int8

// Graph contains connectivity of each vertex
// mapping from [vertexID] => list of adjacent vertices
type Graph []Vertices

var (
	// worker pool will be set to max core number of runtime CPU
	pool *semaphore.Weighted

	// worker is WaitGroup for secondary routine (not main routine)
	worker *sync.WaitGroup

	// Graph description
	graph Graph
	V     int8

	// global minimum
	minimum Vertices = nil
)

// Create graph from file
func createGraph(path string) (Graph, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := file.Close(); err != nil {
			panic(err)
		}
	}()

	scanner := bufio.NewScanner(file)

	// scan number of vertices of the graph
	scanner.Scan()
	nVertices, err := strconv.Atoi(scanner.Text())
	if err != nil {
		panic(err)
	}

	g := make(Graph, nVertices)

	for i := 0; i < len(g); i++ {
		g[i] = make(Vertices, 0)
	}

	// scan number of edges of the graph
	scanner.Scan()
	nEdges, err := strconv.Atoi(scanner.Text())
	if err != nil {
		return nil, err
	}

	// scan all edges information of the graph
	for i := 0; i < nEdges; i++ {
		scanner.Scan()
		vertices := strings.Split(scanner.Text(), " ")

		a, err := strconv.Atoi(vertices[0])
		if err != nil {
			return nil, err
		}

		b, err := strconv.Atoi(vertices[1])
		if err != nil {
			return nil, err
		}

		g[a] = append(g[a], int8(b))
		g[b] = append(g[b], int8(a))
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return g, nil
}

func verify(v int8, prevSpace Vertices, sLength int8, prevSet []bool, count int8) {
	// create new space and append new vertex to the space
	space := make(Vertices, V)
	copy(space, prevSpace)
	space[sLength] = v
	sLength++

	// create new counting set in order to verify the new vertex
	set := make([]bool, V)
	copy(set, prevSet)

	// insert the new vertex to the counting set
	if !set[v] {
		set[v] = true
		count++
	}

	// insert adjacent vertices of the new vertex also
	for _, adjV := range graph[v] {
		if !set[adjV] {
			set[adjV] = true
			count++
		}
	}

	// if counting set is cover all the vertices in graph, update minimum
	if count == V && (minimum == nil || sLength < int8(len(minimum))) {
		minimum = space[:sLength]
		return
	}

	// if not cover all the vertices, search next
	search(space, sLength, set, count)
}

func search(space Vertices, sLength int8, set []bool, count int8) {
	if sLength == V {
		return
	}

	// iterate from next vertex to the last vertex
	for v := space[sLength-1] + 1; v < V; v++ {
		if pool.TryAcquire(1) {
			worker.Add(1)
			go func(v int8) {
				defer pool.Release(1)
				defer worker.Done()
				verify(v, space, sLength, set, count)
			}(v)
		} else {
			verify(v, space, sLength, set, count)
		}
	}
}

func createSearchSpace(v int8) (Vertices, []bool, int8) {
	space := make(Vertices, V)
	space[0] = v

	set := make([]bool, V)
	count := int8(0)

	// insert the vertex to counting set
	set[v] = true
	count++

	// insert adjacent vertices of the vertex also
	for _, adjV := range graph[v] {
		set[adjV] = true
		count++
	}

	return space, set, count
}

func init() {
	fmt.Printf("Runtime CPU: %d vCPU\n", runtime.NumCPU())
	// without hyper threading runtime.NumCPU() is the number of physical core
	runtime.GOMAXPROCS(runtime.NumCPU())
	pool = semaphore.NewWeighted(int64(runtime.NumCPU()))
	worker = &sync.WaitGroup{}
}

func run(file string) {
	var err error

	graph, err = createGraph(file)
	if err != nil {
		panic(err)
	}

	V = int8(len(graph))

	for v := V - 1; v >= 0; v-- {
		space, set, count := createSearchSpace(v)

		if count == V && (minimum == nil) {
			minimum = space[:1]
			break
		}

		if pool.TryAcquire(1) {
			worker.Add(1)
			go func() {
				defer pool.Release(1)
				defer worker.Done()
				search(space, 1, set, count)
			}()
		} else {
			search(space, 1, set, count)
		}
	}

	worker.Wait()
	fmt.Println(minimum)
}

func main() {
	defer func(t time.Time) {
		fmt.Printf("Execution time: %s\n", time.Since(t))
	}(time.Now())

	run(os.Args[1])
}
