module power-grid

go 1.17

require (
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/text v0.3.7
	gonum.org/v1/gonum v0.11.0
)
